package com.tacompress;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.IllegalOptionValueException;
import jargs.gnu.CmdLineParser.UnknownOptionException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

import org.mozilla.javascript.EvaluatorException;

import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

import org.mozilla.javascript.*;

public class TaCompress {
	private static final String JS_EXT = ".js";
	private static final String JS_MIN_EXT = ".min.js";

	private static final String CSS_EXT = ".css";
	private static final String CSS_MIN_EXT = ".min.css";

	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException {
		CmdLineParser parser = new CmdLineParser();
		CmdLineParser.Option dirOption = parser.addStringOption("dir");
		String baseDir = "";
		try {
			parser.parse(args);
			baseDir = (String) parser.getOptionValue(dirOption);

			if (baseDir == null) {
				/* baseDir = "G:\\TaAMP\\www\\dihe\\www\\static"; */
				baseDir = ".";
			}
			File dir = new File(baseDir);

			if (!dir.exists() || !dir.isDirectory()) {
				usage();
				System.exit(1);
			}
		} catch (IllegalOptionValueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownOptionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(baseDir);
		ArrayList<File> files = TaCompress.getListFiles(baseDir);
		removeMinFiles(files);
		buildMinFiles(files);
	}

	public static void usage() {
		System.out.println("ִ�з���:java -jar comparess.jar --dir d:/path/");
	}

	public static void removeMinFiles(ArrayList<File> files) {
		if (!(files instanceof ArrayList)) {
			return;
		}
		String fileString = null;
		for (int i = 0; i < files.size(); i++) {
			File file = files.get(i);
			fileString = file.toString().toLowerCase();
			if (fileString.endsWith(JS_MIN_EXT)
					|| fileString.endsWith(CSS_MIN_EXT)) {
				System.out.println("delete file:" + fileString);
				file.delete();
			}
		}
	}

	public static void buildMinFiles(ArrayList<File> files) {

		if (!(files instanceof ArrayList)) {
			return;
		}
		Reader file_in = null;
		Writer file_out = null;
		String charset = "UTF-8";
		String outFilePath, inFilePath;
		for (int i = 0; i < files.size(); i++) {
			File file = files.get(i);
			if (file.toString().toLowerCase().endsWith(JS_EXT)
					&& !file.toString().toLowerCase().endsWith(JS_MIN_EXT)) {
				inFilePath = file.toString();
				outFilePath = inFilePath.substring(0,
						inFilePath.length() - JS_EXT.length()).concat(
						JS_MIN_EXT);
				/*
				 * System.out.println("js:"); System.out.println(outFilePath);
				 */
				final String localFilename = inFilePath;

				try {

					file_in = new InputStreamReader(new FileInputStream(
							inFilePath), charset);

					JavaScriptCompressor compressor = new JavaScriptCompressor(
							file_in, new ErrorReporter() {

								public void warning(String message,
										String sourceName, int line,
										String lineSource, int lineOffset) {
									System.err.println("\n[WARNING] in "
											+ localFilename);
									if (line < 0) {
										System.err.println("  " + message);
									} else {
										System.err.println("  " + line + ':'
												+ lineOffset + ':' + message);
									}
								}

								public void error(String message,
										String sourceName, int line,
										String lineSource, int lineOffset) {
									System.err.println("[ERROR] in "
											+ localFilename);
									if (line < 0) {
										System.err.println("  " + message);
									} else {
										System.err.println("  " + line + ':'
												+ lineOffset + ':' + message);
									}
								}

								public EvaluatorException runtimeError(
										String message, String sourceName,
										int line, String lineSource,
										int lineOffset) {
									error(message, sourceName, line,
											lineSource, lineOffset);
									return new EvaluatorException(message);
								}
							});
					file_in.close();
					file_in = null;
					file_out = new OutputStreamWriter(new FileOutputStream(
							outFilePath), charset);
					compressor
							.compress(file_out, -1, true, false, false, false);
					file_out.close();
					file_out = null;
					System.out.println("minify js:" + outFilePath);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else if (file.toString().toLowerCase().endsWith(CSS_EXT)
					&& !file.toString().toLowerCase().endsWith(CSS_MIN_EXT)) {
				inFilePath = file.toString();
				outFilePath = inFilePath.substring(0,
						inFilePath.length() - CSS_EXT.length()).concat(
						CSS_MIN_EXT);
				/*
				 * System.out.println("css:"); System.out.println(outFilePath);
				 */
				try {
					file_in = new InputStreamReader(new FileInputStream(
							inFilePath), charset);

					CssCompressor compressor = new CssCompressor(file_in);

					// Close the input stream first, and then open the output
					// stream,
					// in case the output file should override the input file.
					file_in.close();
					file_in = null;
					file_out = new OutputStreamWriter(new FileOutputStream(
							outFilePath), charset);

					compressor.compress(file_out, -1);
					file_out.close();
					file_out = null;
					System.out.println("minify css:" + outFilePath);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	public static ArrayList<File> getListFiles(Object obj) {
		File directory = null;
		if (obj instanceof File) {
			directory = (File) obj;
		} else {
			directory = new File(obj.toString());
		}
		ArrayList<File> files = new ArrayList<File>();
		if (directory.isFile()) {
			files.add(directory);
			return files;
		} else if (directory.isDirectory()) {
			File[] fileArr = directory.listFiles();
			for (int i = 0; i < fileArr.length; i++) {
				File fileOne = fileArr[i];
				files.addAll(getListFiles(fileOne));
			}
		}
		return files;
	}
}
