#TaCompress

这是一个继承YUICompressor的压缩工具。YUICompressor本身只针对一个文件进行压缩。TaCompress只是把找出某个目录下所有的.js和.css文件，并把他们压缩为.min.js和.min.css文件。

[压缩指定目录下的js和css](http://atim.cn/post/1094/)